package com.example.mylibrary.log.define;

/**
 * Msg
 * <br><br>
 * 공통 메시지 Class <br>
 *
 * @author Webcash Smart
 * @since 2017.07
 **/
public class Msg {
	/**
	 * Err
	 * <br><br>
	 * 공통 Err 메시지 정의 <br>
	 *
	 * @author Webcash Smart
	 * @since 2017.07
	 **/
	public class Err {
		/**
		 * SdCard
		 * <br><br>
		 * 저장소 관련 에러 메시지 정의 <br>
		 *
		 * @author Webcash Smart
		 * @since 2017.07
		 **/
		public class SdCard {
			/** SD 카드 미장착 */
			public static final String SDCARD_NOT_EXIST = "SD카드 장착유무를 확인해주세요";
			/** 파일 미존재 */
			public static final String FILE_NOT_EXIST = "파일이 존재하지않습니다.";
			/** 파일 경로 오류 */
			public static final String DIRECTORY_NOT_EXIST = "경로가 올바르지 않습니다.";			
		}

		/**
		 * TxMessage
		 * <br><br>
		 * 공통 전문 에러 메시지 정의 <br>
		 *
		 * @author Webcash Smart
		 * @since 2017.07
		 **/
		public class TxMessage {
			/** 정의되지 않은 전문번호 에러 */
			public static final String TXNO_INVALID = "유효하지 않은 전문번호 입니다.";
		}

		/**
		 * Sql
		 * <br><br>
		 * DB 관련 에러 메시지 정의 <br>
		 *
		 * @author Webcash Smart
		 * @since 2017.07
		 **/
		public class Sql {
			/** DB 처리 도중 오류 발생 */
			public static final String Default 					= "DB처리중 오류가 발생하였습니다";
			/** PK 입력값 미입력 */
			public static final String PRIMARY_KEY_NOT_FOUND 	= " 필수 입력값입니다.";
		}
	}

	/**
	 * Exp
	 * <br><br>
	 * 공통 Exception 메시지 정의 <br>
	 *
	 * @author Webcash Smart
	 * @since 2017.07
	 **/
	public class Exp {
		/** Exception 발생 시 기본 에러메시지 */
		public static final String DEFAULT = "오류가 발생하였습니다";
	}
}
